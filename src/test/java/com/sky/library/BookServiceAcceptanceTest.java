package com.sky.library;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class BookServiceAcceptanceTest {

    private static final String BOOK_REFERENCE = "BOOK-GRUFF472";
    private static final String INVALID_BOOK_REFERENCE = "INVALID-TEXT";
    private static final String UNKNOWN_BOOK_REFERENCE = "BOOK-999";

    private BookFinderService testObj = new BookFinderService(new BookRepositoryStub());

    @Test
    @DisplayName("when retrieving a book by reference that doesn't contain expected prefix then InvalidBookException is thrown")
    void retrieveBookByInvalidBookReferencePrefix() {
        Exception exception = assertThrows(InvalidBookException.class, () -> testObj.retrieveBook(INVALID_BOOK_REFERENCE));
        assertEquals("Book reference must start with: BOOK-", exception.getMessage());
    }

    @Test
    @DisplayName("when retrieving a book by reference that doesn't exists then BookNotFoundException is thrown")
    void retrieveBookByNonExistentReference() {
        assertThrows(BookNotFoundException.class, () -> testObj.retrieveBook(UNKNOWN_BOOK_REFERENCE));
    }

    @Test
    @DisplayName("when retrieving a book by reference that does exist then it is returned")
    void retrieveBookByReference() throws BookNotFoundException {
        assertEquals(BOOK_REFERENCE, testObj.retrieveBook(BOOK_REFERENCE).getReference());
    }

    @Test
    @DisplayName("when retrieving a book summary and book reference is invalid then InvalidBookException is thrown")
    void getBookSummaryByInvalidBookReferencePrefix() {
        Exception exception = assertThrows(InvalidBookException.class, () -> testObj.getBookSummary(INVALID_BOOK_REFERENCE));
        assertEquals("Book reference must start with: BOOK-", exception.getMessage());
    }

    @Test
    @DisplayName("when retrieving a book summary and book is not found then BookNotFoundException is thrown")
    void getBookSummaryByNonExistentReference() {
        assertThrows(BookNotFoundException.class, () -> testObj.getBookSummary(UNKNOWN_BOOK_REFERENCE));
    }

    @Test
    @DisplayName("when retrieving a book summary that does exist then book summary is returned")
    void getBookSummaryByReference() throws BookNotFoundException {
        assertEquals(
            "[BOOK-GRUFF472] The Gruffalo - A mouse taking a walk in the woods.",
            testObj.getBookSummary(BOOK_REFERENCE));
    }

    @Test
    @DisplayName("when retrieving a book summary and review exceeds nine words and contains punctuation then book summary is retrieved")
    void getBookSummaryByLongReviewReferenceAndPunctuation() throws BookNotFoundException {
        assertEquals(
            "[BOOK-POOH222] Winnie The Pooh - In this first volume, we meet all the friends...",
            testObj.getBookSummary("BOOK-POOH222"));
    }

    @Test
    @DisplayName("when retrieving a book summary and review exceeds nine words then book summary is retrieved")
    void getBookSummaryByLongReviewReference() throws BookNotFoundException {
        assertEquals(
            "[BOOK-WILL987] The Wind In The Willows - With the arrival of spring and fine weather outside...",
            testObj.getBookSummary("BOOK-WILL987"));
    }
}
