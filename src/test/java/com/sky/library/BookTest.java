package com.sky.library;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BookTest {

    private static final String REFERENCE = "reference";
    private static final String TITLE = "title";
    private static final String REVIEW = "1 2. 3, 4 5 6 7 8 9";

    @Test
    @DisplayName("when max word count for review field was not exceeded then it is returned correctly")
    void getBookSummaryWhenReviewNotTooLong() {
        Book book = new Book(REFERENCE, TITLE, REVIEW);
        assertEquals("[" + REFERENCE + "] " + TITLE + " - " + REVIEW, book.getSummary());
    }

    @ParameterizedTest(name = "review is correct when max word count for review field was not exceeded and contains trailing punctuation character: \"{0}\"")
    @ValueSource(strings = {",", "."})
    void getBookSummaryWhenReviewIsTooLong(String punctuationCharacter) {
        Book book = new Book(REFERENCE, TITLE, REVIEW + punctuationCharacter + " 10");
        assertEquals("[" + REFERENCE + "] " + TITLE + " - " + REVIEW + "...", book.getSummary());
    }

}
