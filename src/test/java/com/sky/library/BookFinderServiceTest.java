package com.sky.library;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class BookFinderServiceTest {

    private static final String BOOK_REFERENCE = "BOOK-REFERENCE";
    private static final String INVALID_BOOK_REFERENCE = "INVALID";

    @Mock
    BookRepository bookRepository;
    @Mock
    Book book;

    @InjectMocks
    BookFinderService testObj;

    @Test
    @DisplayName("when retrieving a book by reference that does exist then it is returned")
    void retrieveBookByReference() throws BookNotFoundException {
        when(bookRepository.retrieveBook(BOOK_REFERENCE)).thenReturn(book);

        assertEquals(book, testObj.retrieveBook(BOOK_REFERENCE));
    }

    @Test
    @DisplayName("when retrieving a book by invalid reference then InvalidBookException is thrown")
    void retrieveBookByNonExistentReference() {
        Exception exception = assertThrows(InvalidBookException.class, () -> testObj.retrieveBook(INVALID_BOOK_REFERENCE));
        assertEquals("Book reference must start with: BOOK-", exception.getMessage());
    }

    @Test
    @DisplayName("when retrieving a book that doesn't exist then BookNotFoundException is thrown")
    void retrieveBookThatIsNotPresent() {
        when(bookRepository.retrieveBook(BOOK_REFERENCE)).thenReturn(null);

        assertThrows(BookNotFoundException.class, () -> testObj.retrieveBook(BOOK_REFERENCE));
    }

    @Test
    @DisplayName("when retrieving a book summary and book reference is invalid then InvalidBookException is thrown")
    void getBookSummaryByInvalidBookReferencePrefix() {
        assertThrows(InvalidBookException.class, () -> testObj.getBookSummary(INVALID_BOOK_REFERENCE));
    }

    @Test
    @DisplayName("when retrieving a book summary and book is not found then BookNotFoundException is thrown")
    void getBookSummaryByNonExistentReference() {
        when(bookRepository.retrieveBook(BOOK_REFERENCE)).thenReturn(null);

        assertThrows(BookNotFoundException.class, () -> testObj.getBookSummary(BOOK_REFERENCE));
    }

    @Test
    @DisplayName("when retrieving a book summary that does exist then book summary is returned")
    void getBookSummaryByReference() throws BookNotFoundException {
        when(book.getSummary()).thenReturn("summary");
        when(bookRepository.retrieveBook(BOOK_REFERENCE)).thenReturn(book);

        assertEquals("summary", testObj.getBookSummary(BOOK_REFERENCE));
    }

}
