package com.sky.library;

/*
 * Copyright © 2015 Sky plc All Rights reserved.
 * Please do not make your solution publicly available in any way e.g. post in forums or commit to GitHub.
 */

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.joining;

public class Book {

    private static final int MAX_REVIEW_WORD_LENGTH = 9;
    private static final String REVIEW_SUFFIX = "...";
    private static final String PUNCTUATION_REGEX = "\\p{Punct}$";

    private String reference;
    private String title;
    private String review;

    public Book(String reference, String title, String description) {
        this.reference = reference;
        this.title = title;
        this.review = description;
    }

    public String getReview() {
        return isReviewExceedingMaximumWords() ? getTruncatedReview() : review;
    }

    public String getReference() {
        return reference;
    }

    public String getTitle() {
        return title;
    }

    public String getSummary() {
        return "[" + getReference() + "] " + getTitle() + " - " + getReview();
    }

    private boolean isReviewExceedingMaximumWords() {
        return getReviewAsAnArrayOfWords().length > MAX_REVIEW_WORD_LENGTH;
    }

    private String getTruncatedReview() {
        return asList(getReviewAsAnArrayOfWords())
            .subList(0, MAX_REVIEW_WORD_LENGTH)
            .stream()
            .collect(joining(" "))
            .replaceAll(PUNCTUATION_REGEX, "") + REVIEW_SUFFIX;
    }

    private String[] getReviewAsAnArrayOfWords() {
        return this.review.split("\\s");
    }
}
