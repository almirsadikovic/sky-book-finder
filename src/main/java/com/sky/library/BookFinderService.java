package com.sky.library;


import java.util.Optional;

public class BookFinderService implements BookService {

    private final BookRepository bookRepository;

    public BookFinderService(final BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public Book retrieveBook(String bookReference) throws BookNotFoundException {
        validateBookReference(bookReference);
        return Optional.ofNullable(bookRepository.retrieveBook(bookReference)).orElseThrow(BookNotFoundException::new);
    }

    public String getBookSummary(String bookReference) throws BookNotFoundException {
        return retrieveBook(bookReference).getSummary();
    }

    private void validateBookReference(String bookReference) {
        if (!bookReference.startsWith("BOOK-")) {
            throw new InvalidBookException();
        }
    }
}
