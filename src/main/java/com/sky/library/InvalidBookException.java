package com.sky.library;


public class InvalidBookException extends RuntimeException {

    InvalidBookException() {
        super("Book reference must start with: BOOK-");
    }
}
